package br.edu.up.gameengine;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class GameView extends SurfaceView implements Runnable {

  Thread thread = null;
  SurfaceHolder holder;
  Canvas canvas;
  Paint paint;
  Bitmap ghost;

  volatile boolean isJogando;
  boolean isMovendo = false;

  long fps;
  long tempoDoQuadro;
  float velocidade = 150;
  float posicaoInicial = 10;


  // Which ways can the paddle move
  public final int LEFT = 1;
  public final int RIGHT = 2;


  public GameView(Context context) {
    super(context);
    ghost = BitmapFactory.decodeResource(getResources(), R.drawable.ghost1);

    holder = getHolder();
    paint = new Paint();

    isJogando = true;
  }

  @Override
  public void run() {
    while (isJogando){

      long tempoInicial = System.currentTimeMillis();
      atualizar();
      Log.i("TAG", "tá rodando!");
      desenhar();
      tempoDoQuadro = System.currentTimeMillis() - tempoInicial;
      if (tempoDoQuadro > 0){
        fps = 1000 / tempoDoQuadro;
      }

    }
  }

  private void atualizar() {
    if (isMovendo){
      posicaoInicial = posicaoInicial + (velocidade / fps);
    }
  }

  private void desenhar() {

    if (holder.getSurface().isValid()){

      canvas = holder.lockCanvas();
      canvas.drawColor(Color.argb(255,  26, 128, 182));
      paint.setColor(Color.argb(255,  249, 129, 0));
      paint.setTextSize(50);
      canvas.drawText("FPS:" + fps, 20, 40, paint);
      canvas.drawBitmap(ghost,posicaoInicial,200, paint);
      holder.unlockCanvasAndPost(canvas);

    }
  }

  public void pause(){
    isJogando = false;
    try {
        thread.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  public void resume(){
    isJogando = true;
    thread = new Thread(this);
    thread.start();
  }

  @Override
  public boolean onTouchEvent(MotionEvent event) {
    switch (event.getAction() & event.ACTION_MASK){
      case MotionEvent.ACTION_DOWN :
        isMovendo = true;
        break;
      case MotionEvent.ACTION_UP:
        isMovendo = false;
        break;
    }
    return true;
  }
}